package com.gelhosting.absensi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.gelhosting.absensi.config.SessionManagement;

public class SplashActivity extends AppCompatActivity {

    TextView tvSplash;
    Context mContext;
    SessionManagement session;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;
        tvSplash = (TextView) findViewById(R.id.tvSplash);

        session = new SessionManagement(mContext);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(session.isLoggedIn()) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Intent login = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(login);
                }
            }
        }, 2000);
    }
}
