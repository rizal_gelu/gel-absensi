package com.gelhosting.absensi.model;

public class AbsensiData {

    private String employee_id;
    private String GPS_loc_tag;

    public AbsensiData(String employee_id, String GPS_loc_tag) {
        this.employee_id = employee_id;
        this.GPS_loc_tag = this.GPS_loc_tag;
    }

    public String getEmployee_id(){
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getGPS_loc_tag() {
        return GPS_loc_tag;
    }

    public void setGPS_loc_tag(String GPS_loc_tag) {
        this.GPS_loc_tag = GPS_loc_tag;
    }
}
