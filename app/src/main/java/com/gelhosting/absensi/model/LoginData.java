package com.gelhosting.absensi.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginData implements Serializable  {

    public LoginData(){

    }

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("id_user")
    private String id_user;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
