package com.gelhosting.absensi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AbsensiResult {

    public AbsensiResult(){

    }

    @SerializedName("employee_id")
    @Expose
    private String employee_id;

    @SerializedName("GPS_loc_tag")
    private String GPS_loc_tag;

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getGPS_loc_tag() {
        return GPS_loc_tag;
    }

    public void setGPS_loc_tag(String GPS_loc_tag) {
        this.GPS_loc_tag = GPS_loc_tag;
    }
}
