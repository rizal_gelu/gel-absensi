package com.gelhosting.absensi.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gelhosting.absensi.R;
public class About extends Fragment {
    String title;
    int page;

    public static About newInstance(int page, String title) {
        About about = new About();
        Bundle args = new Bundle();
        args.putInt("SomeInt", page);
        args.putString("SomeTitle", title);
        about.setArguments(args);
        return about;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);
        return view;
    }
}
