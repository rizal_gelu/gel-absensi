package com.gelhosting.absensi.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileTabAdapter extends FragmentPagerAdapter {

    private static int numOfTabs = 2;

    public ProfileTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return About.newInstance(0, "About");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return NameCard.newInstance(1, "Name Card");
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
