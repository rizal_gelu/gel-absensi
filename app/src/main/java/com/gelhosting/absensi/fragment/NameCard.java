package com.gelhosting.absensi.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gelhosting.absensi.LoginActivity;
import com.gelhosting.absensi.MainActivity;
import com.gelhosting.absensi.R;
import com.gelhosting.absensi.RetrofitInterface;
import com.gelhosting.absensi.adapter.NameCardAdapter;
import com.gelhosting.absensi.config.ApiClient;
import com.gelhosting.absensi.config.SessionManagement;
import com.gelhosting.absensi.model.LoginData;
import com.gelhosting.absensi.model.ResponseServer;
import com.gelhosting.absensi.model.User;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class NameCard extends Fragment {
    String title;
    int page;
    SessionManagement session;
    private RecyclerView recyclerView;
    private RetrofitInterface apiInterface;
    LinearLayoutManager mLayoutManager;
//    private NameCardAdapter adapter;

    private BottomNavigationView.OnNavigationItemSelectedListener botnav = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_camera:
//                    ScanQrCode();
                    return true;
                case R.id.navigation_person:
                    return true;
            }
            return false;
        }
    };

    public static NameCard newInstance(int page, String title) {
        NameCard nameCard = new NameCard();
        Bundle args = new Bundle();
        args.putInt("SomeInt", page);
        args.putString("SomeTitle", title);
        nameCard.setArguments(args);
        return nameCard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_cardname, container, false);
//        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setHasFixedSize(true);

//        apiInterface = ApiClient.getClient().create(RetrofitInterface.class);
//        String id_user = "0e606a1d-6488-542e-801f-44e6867bb63d";
//        apiInterface.getUser(id_user)
//                .enqueue(new Callback<ResponseServer>() {
//                    @Override
//                    public void onResponse(retrofit2.Call<ResponseServer> call, Response<ResponseServer> response) {
//                        ResponseServer resource = response.body();
//                        List<User> userList = resource.getData();
//                        NameCardAdapter adapter = new NameCardAdapter(userList);
//                        recyclerView.setAdapter(adapter);
//                    }
//
//                    @Override
//                    public void onFailure(retrofit2.Call<ResponseServer> call, Throwable t) {
//                        call.cancel();
//                    }
//                });
        return view;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//    }
}
