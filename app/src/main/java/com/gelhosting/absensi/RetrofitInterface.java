package com.gelhosting.absensi;

import com.gelhosting.absensi.model.AbsensiResult;
import com.gelhosting.absensi.model.ForgotPassword;
import com.gelhosting.absensi.model.LoginData;
import com.gelhosting.absensi.model.RegisterData;
import com.gelhosting.absensi.model.ResponseServer;
import com.gelhosting.absensi.model.User;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface RetrofitInterface {
//    @POST("api/example/absensi")
//    Call<AbsensiResult> getStringScalar(@Query("employee_id") String employee_id, @Query("GPS_loc_tag") String GPS_loc_tag);

    @POST("api/example/absensi")
    @FormUrlEncoded
    Call<AbsensiResult> getStringScalar(@Field("employee_id") String employee_id, @Field("GPS_loc_tag") String GPS_loc_tag);

    @FormUrlEncoded
    @POST("api/example/login")
    Call<LoginData> getLogin(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/example/register")
    Call<RegisterData>postRegister(@Field("first_name") String first_name, @Field("last_name") String last_name,
                                   @Field("email") String email, @Field("password") String Password,
                                   @Field("phone") String phone);

    @FormUrlEncoded
    @POST("api/example/absensi")
    Call<AbsensiResult>postAbsensi(@Field("GPS_loc_tag") double GPS_loc_tag, @Field("employee_id") String employee_id);

    @FormUrlEncoded
    @POST("api/example/forgot_password")
    Call<ForgotPassword>forgotPassword(@Field("identity") String identity);

    @GET("api/example/karyawan")
    Call<ResponseServer>getUser(@Query("id_user") String id_user);
}
