package com.gelhosting.absensi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.gelhosting.absensi.config.ApiClient;
import com.gelhosting.absensi.model.ForgotPassword;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    Button btnreset;
    ImageView logoJadi;
    EditText idenitity;
    Context mContext;
    RetrofitInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mContext = this;

        apiInterface    = ApiClient.getClient().create(RetrofitInterface.class);
        logoJadi        = (ImageView) findViewById(R.id.logoJadi);
        idenitity        = (EditText) findViewById(R.id.idenitity);
        btnreset        = (Button) findViewById(R.id.btnreset);
        
        btnreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPasswordProses();
            }
        });
    }

    private void resetPasswordProses() {
        apiInterface.forgotPassword(idenitity.getText().toString()).enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                if(response.isSuccessful()) {
//                            Toast.makeText(mContext, "Silahkan Login", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(mContext, LoginActivity.class));
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.getString("error").equals("false")) {
                            Toast.makeText(mContext, "Berhasil Register", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(mContext, LoginActivity.class));
                        } else {
                            String error_message = jsonObject.getString("error_msg");
                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, "Error Tayyy", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                call.cancel();
            }
        });
    }
}
