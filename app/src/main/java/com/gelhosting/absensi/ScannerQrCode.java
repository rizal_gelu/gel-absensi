package com.gelhosting.absensi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.gelhosting.absensi.config.SessionManagement;
import com.gelhosting.absensi.model.AbsensiResult;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScannerQrCode extends AppCompatActivity {

    Context mContext;
    IntentIntegrator intentIntegrator;

    RetrofitInterface apiInterface;
    SessionManagement session;

    LocationManager locationManager;
    Location location;
    double LatLong;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        session = new SessionManagement(mContext);
        intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(mContext, "Hasil Tidak ditemukan", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, result.getContents(), Toast.LENGTH_SHORT).show();

                // Mulai dari sini
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    }, 101);

                    try {
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                                5000, 5, (LocationListener) this);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }

//                    LatLong = location.getLatitude() + location.getLongitude();
                }
                LatLong = location.getLatitude() + location.getLongitude();

                apiInterface.postAbsensi(LatLong, result.getContents())
                        .enqueue(new Callback<AbsensiResult>() {
                            @Override
                            public void onResponse(Call<AbsensiResult> call, Response<AbsensiResult> response) {
                                if(response.isSuccessful()) {
                                    Toast.makeText(mContext, "Berhasil Absen", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mContext, "Maaf terjadi kesalahan, coba lagi", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AbsensiResult> call, Throwable t) {

                            }
                        });

                try{
                    JSONObject object = new JSONObject(result.getContents());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
