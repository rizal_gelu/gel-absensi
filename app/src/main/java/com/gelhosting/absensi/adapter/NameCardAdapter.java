package com.gelhosting.absensi.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gelhosting.absensi.R;
import com.gelhosting.absensi.model.User;

import java.util.ArrayList;
import java.util.List;

public class NameCardAdapter extends RecyclerView.Adapter<NameCardAdapter.ViewHolder> {

    public List<User> data;
    public Activity listContext;

    public NameCardAdapter(List<User> data) {
        this.data = data;
    }

    @Override
    public NameCardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_cardname, viewGroup, false);
//        return new ViewHolder(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NameCardAdapter.ViewHolder viewHolder, int i) {
        viewHolder.companyName.setText(data.get(i).getCompany());
        viewHolder.posisiName.setText(data.get(i).getUsername());
        viewHolder.posisiEmail.setText(data.get(i).getEmail());
        viewHolder.posisiPhone.setText(data.get(i).getPhone());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView companyName, posisiName, posisiEmail, posisiPhone;
        private  ImageView photoProfile, QrCode;

        public ViewHolder(View viewlist) {
            super(viewlist);

            companyName = viewlist.findViewById(R.id.companyName);
            posisiName = viewlist.findViewById(R.id.posisiName);
            posisiEmail = viewlist.findViewById(R.id.posisiEmail);
            posisiPhone = viewlist.findViewById(R.id.posisiPhone);
        }
    }
}
