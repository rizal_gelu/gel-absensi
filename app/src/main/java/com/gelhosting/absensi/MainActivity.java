package com.gelhosting.absensi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gelhosting.absensi.config.SessionManagement;

//Current Location
import android.content.Context;
import android.view.MenuItem;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    Context mContext;
    RetrofitInterface apiInterface;
    SessionManagement session;

    private TextView text;
    private BottomNavigationView.OnNavigationItemSelectedListener botnav = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    text.setText(R.string.title_home);
                    return true;
                case R.id.navigation_camera:
                    Intent i = new Intent(MainActivity.this, ScannerQrCode.class);
                    startActivity(i);
                    return true;
                case R.id.navigation_person:
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        session = new SessionManagement(mContext);

        text = (TextView) findViewById(R.id.text);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigasi);
        bottomNavigationView.setOnNavigationItemSelectedListener(botnav);
    }
}

